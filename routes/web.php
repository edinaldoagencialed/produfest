<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

// Route::get('/importa/categorias', ['as'=>'home','uses'=>'ImportController@categories']);
// Route::get('/importa/subcategorias', ['as'=>'home','uses'=>'ImportController@subcategories']);

//Route::get('/importa/produtos', ['as'=>'home','uses'=>'ImportController@products']);

Route::group(['middleware' => 'globaldatas', 'as' => 'front::'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);
    Route::get('/sobre', ['as' => 'about', 'uses' => 'PagesController@about']);

    Route::get('/catalogo', ['as' => 'catalogo', 'uses' => 'PagesController@catalogo']);
    Route::get('/contato', ['as' => 'contato', 'uses' => 'PagesController@contato']);
    Route::get('/produtos', ['as' => 'produtos', 'uses' => 'PagesController@produtos']);

    Route::get('/produto/{url}', ['as' => 'product', 'uses' => 'ProductsController@detail']);
    Route::get('/pesquisa', ['as' => 'search', 'uses' => 'ProductsController@search']);

    Route::get('/categoria/{url}', ['as' => 'categories', 'uses' => 'CategoriesController@index']);
    Route::get('/subcategoria/{url}', ['as' => 'subcategories', 'uses' => 'SubCategoriesController@index']);

    Route::post('/enviar/catalogo', ['as' => 'sendmailcatalogo', 'uses' => 'EmailController@sendmailcatalogo']);
    Route::post('/enviar/email', ['as' => 'sendmail', 'uses' => 'EmailController@email']);
    Route::post('/enviar/newsletter', ['as' => 'newsletter', 'uses' => 'EmailController@newsletter']);

    //  Route::get('/catalogo',['as'=>'catalogo','uses'=>'PagesController@catalog'])
    //  Route::get('/categoria/{url}',['as'=>'categories','uses'=>'CategoriesController@index']);
    //  Route::get('/produto/urltransform',['as'=>'urltransform','uses'=>'ProductsController@urltransform']);
    //  Route::get('/produto/{url}',['as'=>'single_product','uses'=>'ProductsController@single_product']);
    //  Route::get('/pesquisa',['as'=>'search','uses'=>'ProductsController@search']);
    //  Route::post('/enviar/email',['as'=>'sendmail','uses'=>'PagesController@email']);
    //  Route::post('/enviar/newsletter',['as'=>'newsletter','uses'=>'PagesController@newsletter']);
    //  Route::get('/categorias',['as'=>'produtos','uses'=>'ProductsController@categorias']);
    //  Route::get('/produtos',['as'=>'produtos','uses'=>'ProductsController@produtos']);

});
