function confirmDelete(link) {
    swal({
        title: 'Confirmação!',
        text: "Deseja realmente apagar o registro?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Apagar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result) {
            location.href = link;
        }
    });
}


$(function () {
    $('.personalmask').attr('maxlength', '18');
    $('.phonemask').attr('maxlength', '15');
    $('.cepmask').attr('maxlength', '9');

    $('.onlynumber').keypress(function () {
        var code = (event.keyCode ? event.keyCode : event.which);
        if ((code > 47 && code < 58))
            return true;
        else {
            if (code == 8 || code == 0)
                return true;
            else
                return false;
        }
    });

    /* Aplicando mascara de CPF/CNPJ */
    $('.personalmask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if (v.length <= 13) { //CPF
            v = v.replace(/(\d{3})(\d)/, "$1.$2");
            v = v.replace(/(\d{3})(\d)/, "$1.$2");
            v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            $(this).val(v);
        } else { //CNPJ
            v = v.replace(/^(\d{2})(\d)/, "$1.$2");
            v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
            v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
            v = v.replace(/(\d{4})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    /* Aplicando Mascara de Telefones/Celular */
    $('.phonemask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 10) {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{4})(\d)/, "$1-$2");
            $(this).val(v);
        } else {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{5})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    /* Aplicando mascara de CEP */

    $('.cepmask').keyup(function () {
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 8) {
            v = v.replace(/^(\d{5})(\d)/, "$1-$2");
            $(this).val(v);
        }
    });

    $('.datemask').keyup(function () {
        $(this).attr('maxlength', '10');
        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        if ($(this).val().length <= 10) {
            v = v.replace(/^(\d{2})(\d)/, "$1/$2");
            v = v.replace(/(\d{2})(\d)/, "$1/$2");
            $(this).val(v);
            /*Validade se data é valida*/
        }
    });

    $('.datemask').on('blur', function () {

        var regex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;

        var txt = $(this).val();

        if (!regex.test(txt)) {
            $(this).val('');
            $(this).focus();
        }

    })


    /*Consultar CEP*/
    $('.cepconsult').blur(function () {
        /* Configura a requisição AJAX */
        if ($(this).val() !== "") {
            $.ajax({
                url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/', /* URL que será chamada */
                type: 'GET', /* Tipo da requisição */
                success: function (data) {
                    if (data) {
                        $('input[name="uf"]').val(data.uf);
                        $('input[name="cidade"]').val(data.localidade);
                    } else {

                    }
                }
            });
        }
        return false;
    })
});
