@extends('layouts.default')
@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area mb-78">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('front::home') }}">Produfest</a></li>
                        <li>Catálogo</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->

<!--contact area start-->
<div class="contact_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="contact_message content">
                    <h3>Observação</h3>
                    <p>Preencha o formulário ao lado e receba no e-mail cadastrado o nosso catálogo completo.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="contact_message form">
                    <h3>Deixe seu contato aqui</h3>
                    <form id="contact" method="POST" action="{{ route('front::sendmailcatalogo') }}">
                        @csrf
                        <p>
                            <label>Nome*:</label>
                            <input name="nome" placeholder="" type="text" required>
                        </p>
                        <p>
                            <label>E-mail*:</label>
                            <input name="email" placeholder="" type="email" required>
                        </p>
                        <p>
                            <label>CNPJ*:</label>
                            <input name="cnpj" class="personalmask" placeholder="" type="text" required>
                        </p>
                        <button type="submit"> Enviar</button>
                        <p class="form-messege"></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<!--contact area end-->

<!--map js code here-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdWLY_Y6FL7QGW5vcO3zajUEsrKfQPNzI"></script>
<script src="https://www.google.com/jsapi"></script>
<script src="{{ asset('js/map.js') }}"></script>

@endsection