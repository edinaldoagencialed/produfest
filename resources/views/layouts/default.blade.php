<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @if(!isset($_subtitle))
    <title>{{ $_title }}</title>
    @else
    <title>{{ $_subtitle }} | {{ $_title }}</title>
    @endif

    <link rel="icon" type="image/ico" href="{{ asset('img/favicon.ico') }}" />

    <!-- Main CSS and Plugins -->
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144023521-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144023521-1');
    </script>

    @include('blocks.header')
    @yield('content')
    @include('blocks.footer')


    <!-- Mains JS and Plugins -->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
    <script>
        $(function(){

                if($('#newsletter').length>0){
                    $('#newsletter').submit(function(){
                        $.ajax({
                            url:"{{ route('front::newsletter') }}",
                            type:'post',
                            data: $(this).serialize(),
                            success:function(res){
                                if(res==1){
                                    alert('E-mail cadastrado com sucesso!');
                                    $('#newsletter')[0].reset();
                                }else if(res==2){
                                    alert('Seu e-mail já foi cadastrado!');
                                    $('#newsletter')[0].reset();
                                }else{
                                    alert('Ocorreu um erro ao cadastrar o seu e-mail');
                                }
                            }
                        });
                        return false;
                    });
                }

                if($('#contact').length>0){
                    $('#contact').submit(function(){
                        $.ajax({
                            url:$(this).attr('action'),
                            type:'post',
                            data: $(this).serialize(),
                            success:function(res){
                                if(res==1){
                                    $('p.form-messege').html('E-mail enviado com sucesso!');
                                    $('#contact')[0].reset();
                                    setTimeout(function(){
                                        $('p.form-messege').html('');
                                    },5000);
                                }else{
                                    $('p.form-messege').html('Ocorreu um erro ao enviar seu e-mail.');
                                    $('#contact')[0].reset();
                                    setTimeout(function(){
                                        $('p.form-messege').html('');
                                    },5000);
                                }
                            }
                        });
                        return false;
                    });
                }

                var zoom = $(".zoomWrapper>a>img#zoom1").elevateZoom({
                    gallery:'gallery_01', 
                    responsive : true,
                    cursor: 'crosshair',
                    zoomType : 'inner'
                });  

                $('ul.variations li a').on('click',function(e){
                    e.preventDefault();
                    image_change = $(this).data('image');
                    // $('#zoom1').attr('src',image_change);
                    // $('#zoom1').data('zoom-image',image_change);
                    //$('.zoomWrapper>a>img#zoom1').remove();
                    zoom.attr('src', image_change);
                    zoom.data('zoom-image',image_change);
                    $(".zoomWrapper>a>img#zoom1").elevateZoom({
                        gallery:'gallery_01', 
                        responsive : true,
                        cursor: 'crosshair',
                        zoomType : 'inner'
                    });  
                });
            });
    </script>
</body>

</html>