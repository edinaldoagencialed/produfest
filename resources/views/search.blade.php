@extends('layouts.default')
@section('content')
<!-- CATEGORY NAME -->
    <section class="category-name" style="background-color:#bc2516;">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h4 class="subcategory-title">Pesquisando por: </h4>
					<h2 class="category-title">{{ $term }}</h2> 
				</div>
			</div>
		</div>
	</section>

	<!-- PRODUCT LOOP -->
	<section class="product-loop">
		<div class="container">
			<div class="row">
				@if (count($result)>0)
					@foreach($result as $row)
					<div class="col-md-4 product-loop-item">
						<a href="{{ route('front::single_product',['url'=>$row->url ]) }}">
							<img src="{{ URL::to('uploads/produtos/'.$row->image) }}" alt="img-ballon" class="img-responsive">
							<div class="product-title">
								<h4 class="product-loop-title">{{ $row->sku }}</h4>
								<h5 class="product-loop-description">{{ $row->name }}</h5>
							</div>
						</a>
						@if($row->description!="")
							<div class="product-description">
								<p class="init-desc">Descrição: </p>
								<p>
										{!! $row->description !!}
								</p>
							</div>    
						@endif
					</div>
					@endforeach
				@else
					Nenhuma produto encontrado
				@endif
			</div>
		</div>
	</section>
  
@endsection