<style>
    section.product-loop button {
        border-color: {{ $category->color }};
        color: {{ $category->color }};
        background-color: transparent;
        text-transform: uppercase;
        }

    section.product-loop button:hover {
        color: #fff;
        background-color: {{ $category->color }};
        transition: 0.2s ease;
        }
</style>
