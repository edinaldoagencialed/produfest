<!--newsletter area start-->
<div class="newsletter_area mb-60">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="newsletter_container">
                    <div class="newsletter_title">
                        <h3>SEJA UM REVENDEDOR</h3>
                        <p>Deixe seu e-mail que entraremos em contato.</p>
                    </div>
                    <div class="subscribe_form">
                        <form id="newsletter" class="mc-form footer-newsletter" >
                                @csrf
                            <input id="mc-email" name="email" type="email" autocomplete="off" placeholder="Digite seu e-mail" />
                            <button id="mc-submit">INSCREVER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--newsletter area end-->

<!--footer area start-->
<footer class="footer_widgets">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6">
                    <div class="widgets_container">
                        <h3>Siga-nos</h3>
                        <div class="footer_social">
                            <ul>
                                <li><a href="http://www.facebook.com/GRUPRODUFEST/" target="_BLANK"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                                <li><a href="http://www.instagram.com/produfestrpoficial/" target="_BLANK"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <div class="widgets_container widget_menu">
                        <h3>Endereço</h3>
                        <div class="footer_contact">
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>Estrada Vicinal João Parise, 1930 <br /> Chácara Jockey Club <br /> São José do Rio Preto/SP</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <div class="widgets_container widget_menu">
                        <h3>Telefone</h3>
                        <div class="footer_contact">
                            <ul>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p><a href="tel:1732388455">(17) 3238-8455</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <div class="widgets_container widget_menu">
                        <h3>E-mail</h3>
                        <div class="footer_contact">
                            <ul>
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <p>contato@produfest.com.br</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area">
                        <p>Copyright &copy; {{ now()->year }} <a href="{{ route('front::home') }}">Produfest</a> - Todos os direitos reservados.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area text-right">
                        <p>Desenvolvido por <a href="http://leddigital.com.br">LED Digital</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
