<!-- MAIN MENU -->
<!--header area start-->

<!--Offcanvas menu area start-->
<div class="off_canvars_overlay">

</div>
<div class="Offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                </div>
                <div class="Offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                    </div>
                    <div class="header_right_info">
                        <ul>
                            <li class="search_box"><a href="javascript:void(0)"><i class="pe-7s-search"></i></a>
                                <div class="search_widget">
                                    <form action="{{ route('front::search') }}">
                                        <input placeholder="Procure por nossos produtos" name='s' type="text">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            <li class="menu-item-has-children active">
                                <a href="{{ route('front::home') }}">Home</a>
                            </li>
                            <li class="menu-item-has-children active">
                                <a href="{{ route('front::about') }}">Produfest</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:;">Produtos</a>
                                <ul class="sub-menu">


                                    @foreach ($_categories as $item)
                                    <li class="menu-item-has-children">
                                        <a
                                            href="{{ route('front::categories',['url'=>$item->url]) }}">{{ $item->name }}</a>
                                        <ul class="sub-menu">
                                            @foreach ($item->subcategories as $sub)
                                            <li><a
                                                    href="{{ route('front::subcategories',['url'=>$sub->url]) }}">{{ $sub->name }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach

                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ route('front::catalogo') }}">Catálogo</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ route('front::contato') }}">Contato</a>
                            </li>
                        </ul>
                    </div>

                    <div class="Offcanvas_footer">
                        <span><a href="mailto:contato@produfest.com.br"><i
                                    class="fa fa-envelope-o"></i>contato@produfest.com.br</a></span>
                        <ul>
                            <li class="facebook"><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                            <li class="instagram"><a href="index.html#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Offcanvas menu area end-->

<header>
    <div class="main_header sticky-header">
        <div class="container">
            <div class="header_container info-top">
                <div class="row">
                    <div class="col-lg-6 ">
                        <i class="fa fa-envelope-o"></i> <a
                            href="{{ route('front::contato') }}">contato@produfest.com.br</a>
                    </div>
                    <div class="col-lg-6 text-right">
                        <i class="fa fa-whatsapp"></i> <a href="https://web.whatsapp.com/send/?phone=5517991015109"
                            target="_blank">(17)
                            99101-5109</a>
                    </div>
                </div>
            </div>
            <div class="header_container">
                <div class="row align-items-center">
                    <div class="col-lg-2">
                        <div class="logo">
                            <a href="{{ route('front::home') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <!--main menu start-->
                        <div class="main_menu menu_position">
                            <nav>
                                <ul>
                                    <li>
                                        <a class="active" href="{{ route('front::home') }}">home</a>
                                    </li>
                                    <li><a href="{{ route('front::about') }}">Produfest</a></li>
                                    <li class="mega_items"><a href="javascript:;">Produtos<i
                                                class="fa fa-angle-down"></i></a>
                                        <div class="mega_menu">
                                            <ul class="mega_menu_inner">
                                                @foreach ($_categories as $item)
                                                <li>
                                                    <a
                                                        href="{{ route('front::categories',['url'=>$item->url]) }}">{{ $item->name }}</a>
                                                    <ul>
                                                        @foreach ($item->subcategories as $sub)
                                                        <li><a
                                                                href="{{ route('front::subcategories',['url'=>$sub->url]) }}">{{ $sub->name }}</a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="{{ route('front::catalogo') }}">Catálogo</a>
                                    </li>
                                    <li><a href="{{ route('front::contato') }}">Contato</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!--main menu end-->
                    </div>
                    <div class="col-lg-2">
                        <div class="header_right_info">
                            <ul>
                                <li class="search_box"><a href="javascript:void(0)"><i class="pe-7s-search"></i></a>
                                    <div class="search_widget">
                                        <form action="{{ route('front::search') }}">

                                            <input placeholder="Pesquise nossos produtos" name='s' type="text">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!--header area end-->