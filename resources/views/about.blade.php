@extends('layouts.default')
@section('content')


<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ route('front::home') }}">Produfest</a></li>
                            <li>Sobre</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--about section area -->
    <div class="about_page_section">
       <div class="container">
            <!--about section area -->
            <div class="about_section">
                <div class="row">
                    {{-- <div class="col-lg-6 col-md-12">
                        <div class="about_thumb">
                            <img src="assets/img/about/about1.jpg" alt="">
                        </div>
                    </div> --}}
                    <div class="col-lg-12 col-md-12">
                        <div class="about_content">
                            <h1>Sobre nós</h1>
                            <p class="about-us-text">A Produfest surgiu com o intuito de inovar, modernizar e agregar valor às datas
                                    comemorativas, aos momentos mais importantes, que pedem uma celebração.</p>
                                <p class="about-us-text">Com alta tecnologia em moldes e injeção, desenvolve artigos de plástico para
                                    festas. Uma estrutura completa e moderna foi desenvolvida para atender o consumidor final, empresas
                                    e lojistas de todo o Brasil.</p>
                                <p class="about-us-text">Por desenvolver seus próprios moldes, a ProduFest garante preços mais
                                    acessíveis e prazos menores nas entregas. Tudo pensando para oferecer o melhor custo-benefício do
                                    mercado com variedade e qualidade de produtos para eventos e comemorações.</p>
                                <p class="about-us-text">Conheça nosso catálogo e realize os melhores momentos dos seus clientes.</p>
                                <p class="about-us-text">Boas vendas!</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--about section end-->

            
        </div>
    </div>
    <!--about section end-->

@endsection
