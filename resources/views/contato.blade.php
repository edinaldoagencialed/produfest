@extends('layouts.default')
@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area mb-78">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('front::home') }}">Produfest</a></li>
                        <li>Fale Conosco</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--breadcrumbs area end-->

    <!--contact map start-->
    {{-- <div class="contact_map mt-60">
        <div class="map-area">
            <div id="googleMap"></div>
        </div>
    </div> --}}
    <!--contact map end-->

    <!--contact area start-->
    <div class="contact_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="contact_message content">
                        <h3>Fale Conosco</h3>
                            <p>Entre em contato conosco para tirar dúvidas e saber mais informações sobre a nossa empresa.</p>
                        <ul>
                            <li><strong>Endereço:</strong> <br />Estrada Vicinal João Parise nº 1930 <br /> 
                                Chácara Jockey Club <br /> CEP 15062-000 <br />  São José do Rio Preto/SP</li>
                            <li><i class="fa fa-whatsapp"></i><a href="https://web.whatsapp.com/send/?phone=5517991015109" target="_blank">(17) 99101-5109</a></li>
                            <li><i class="fa fa-envelope-o"></i><a href="mailto:contato@produfest.com.br">contato@produfest.com.br</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="contact_message form">
                        <h3>Mande uma mensagem</h3>
                        <form id="contact" method="POST" action="{{ route('front::sendmail') }}">
                            @csrf
                            <p>
                                <label>Nome*:</label>
                                <input name="name" placeholder="" type="text" required>
                            </p>
                            <p>
                                <label>E-mail*:</label>
                                <input name="email" placeholder="" type="email" required>
                            </p>
                            <p>
                                <label>Assunto</label>
                                <input name="subject" placeholder="" type="text" required>
                            </p>
                            <div class="contact_textarea">
                                <label>Mensagem*:</label>
                                <textarea name="message_contact" class="form-control2" required></textarea>
                            </div>
                            <button type="submit"> Enviar</button>
                            <p class="form-messege"></p>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--contact area end-->

    <!--map js code here-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdWLY_Y6FL7QGW5vcO3zajUEsrKfQPNzI"></script>
    <script  src="https://www.google.com/jsapi"></script>
    <script src="{{ asset('js/map.js') }}"></script>

@endsection





