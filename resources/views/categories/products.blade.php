@extends('layouts.default')
@section('content')

<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('front::home') }}">Produfest</a></li>
                        <li>{{ $category->name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->

<!--shop  area start-->
<div class="shop_area mt-60 mb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <!--sidebar widget start-->
                <aside class="sidebar_widget">
                    <div class="widget_inner">
                        <div class="widget_list widget_categories">
                            <h3>Subcategorias</h3>
                            <ul class="widget_dropdown_categories dropdown_categories1">

                                @if ($category->subcategories->count()>0)
                                @foreach ($category->subcategories as $item)
                                <li><a {{ ($item->url==$category->url)?"'/class=subcategorie_active":'' }}
                                        href="{{ route('front::subcategories',['url'=>$item->url]) }}">{{ $item->name }}</a>
                                </li>
                                @endforeach
                                @else
                                Nada encontrado!
                                @endif
                            </ul>
                        </div>
                        <div class="widget_list widget_categories">
                            <h3>Categorias</h3>
                            <ul class="widget_dropdown_categories dropdown_categories1">
                                @foreach ($_categories as $item)
                                <li>
                                    <a {{ ($item->url==$category->url)?"'/class=subcategorie_active":'' }} href="{{ route('front::categories',['url'=>$item->url]) }}">
                                        {{ $item->name }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </aside>
                <!--sidebar widget end-->
            </div>
            <div class="col-lg-9 col-md-12">
                <h4>{{ $category->name }}</h4>
                <!--shop toolbar start-->
                <div class="shop_toolbar_wrapper">
                    <div class="shop_toolbar_btn">
                        <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip"
                            title="3"></button>
                        <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip"
                            title="4"></button>
                    </div>
                    <div class="page_amount">
                        <p>
                            Página {{ $products->currentPage() }} de {{ $products->lastPage() }}
                        </p>
                    </div>
                </div>
                <!--shop toolbar end-->
                <div class="row shop_wrapper">
                    @foreach ($products as $item)
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <a href="{{ route('front::product',['url'=>$item->url]) }}">
                                    <div class="product_thumb">
                                        @php
                                        $file = public_path().'/uploads/produtos/'.$item->image;
                                        @endphp

                                        @if(file_exists($file))
                                        <img src="{{ URL::to('uploads/produtos/'.$item->image) }}" alt="">
                                        @else
                                        <img src="{{ URL::to('uploads/produtos/noimage.jpg') }}" alt="$item->image">
                                        @endif
                                    </div>
                                </a>
                                <div class="product_content grid_content">
                                    <h4 class="product_name"><a
                                            href="{{ route('front::product',['url'=>$item->url]) }}">{{ $item->name }}</a>
                                    </h4>
                                </div>
                            </figure>
                        </article>
                    </div>
                    @endforeach
                </div>

                @if ($products->hasPages())
                <div class="shop_toolbar t_bottom">
                    <div class="pagination">
                        {{ $products->links() }}
                    </div>
                </div>
                @endif
                <!--shop toolbar end-->
                <!--shop wrapper end-->
            </div>

        </div>
    </div>
</div>


<!--shop  area end-->
@endsection