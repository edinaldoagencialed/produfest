@extends('layouts.default')
@section('content')
<section class="categories-list">
        <div class="container">
        <div class="categories-content">
            <div class="row">
                <div class="col-md-12 text-center vcenter" style=" background:{{ $category->color }}">
                    
                    <h2 class="product-list-title" style="z-index: 999; color:#FFFFFF;">{{ $category->name }}</h2>
                </div>
            </div>
            
            @php
            $_columns = 0;
            @endphp
            @foreach ($subcategories as $sub)
            @php
            $_size = $sub->columns;
            @endphp
            
            @if($_columns==12 or $_columns==0)
            <div class="row vertical-align">
                @endif
                @if($sub->image!="")
                <div data-url="{{ route('front::categories',['url'=>$sub->url]) }}"
                    class="col-md-{{ $_size }} text-center categories-item"
                    style="background:url('{{ URL::to('uploads/categorias/'.$sub->image) }}') {{ $sub->color }}; background-size: cover;">
                    <div class="msk" style="background: {{ $sub->color }}">
                    </div>
                    @else
                    <div data-url="{{ route('front::categories',['url'=>$sub->url]) }}"
                        class="col-md-{{ $_size }} text-center categories-item" style="background:{{ $sub->color }}">
                        @endif
                        <h2 class="product-list-title">{{ $sub->name }}</h2>
                    </div>
                    @php
                    $_columns = $_columns + $_size;
                    @endphp
    
                    @if($_columns==12)
                </div>
                @php
                $_columns = 0;
                @endphp
                @endif
                @endforeach
            </div>
            <script>
                    $(document).ready(function(){
                        $('.vertical-align > [class^="col-"]').on('click',function(){
                            location.href=$(this).data('url');
                        })
                    });
                </script>
                </div>
</section>
@endsection