@extends('layouts.default')
@section('content')

<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ route('front::home') }}">Produfest</a></li>
                            <li>{{ $_product->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

<!--product details start-->
<div class="product_details mt-60 mb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product-details-tab">
                    <div id="img-1" class="zoomWrapper single-zoom">
                        <a href="javascript:;">
                            @php
                                $file = public_path().'/uploads/produtos/'.$_product->image;
                            @endphp
                            @if(file_exists($file))
                                <img id="zoom1" src="{{ URL::to('uploads/produtos/'.$_product->image) }}" data-zoom-image="{{ URL::to('uploads/produtos/'.$_product->image) }}" alt="{{ $_product->image }}" title="{{ $_product->image }}">
                            @else
                                <img id="zoom1" src="{{ URL::to('uploads/produtos/noimage.jpg') }}" data-zoom-image="{{ URL::to('uploads/produtos/noimage.jpg') }}" alt="" alt="{{ $_product->image }}" title="{{ $_product->image }}">
                            @endif
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product_d_right">
                        <h1>{{ $_product->name }}</h1>
                        <div class="product_variant color">
                            @if ($_product->description!="")
                                <h3>Descrição</h3>
                                <p>{!! $_product->description !!}</p>
                            @endif
                            
                            
                            
                            <h3>Opções disponíveis</h3>
                            <small>Clique na imagem para ampliar</small>
                            <ul class="variations">
                                @foreach ($_product->variations as $item)
                                <li>
                                    @php
                                        $file = public_path().'/uploads/produtos/'.$item->image;
                                    @endphp
                                    @if(file_exists($file))
                                        <a href="javascript:;" data-image="{{ URL::to('uploads/produtos/'.$item->image) }}">
                                            <img src="{{ URL::to('uploads/produtos/'.$item->image) }}" data-zoom-image="{{ URL::to('uploads/produtos/'.$item->image) }}" alt="">
                                        </a>
                                    @else
                                        <a href="javascript:;" data-image="{{ URL::to('uploads/produtos/noimage.jpg') }}">
                                            <img src="{{ URL::to('uploads/produtos/noimage.jpg') }}" alt="{{ $item->sku }}">
                                        </a>
                                    @endif
                                </li>
                                @endforeach
                                
                               
                            </ul>
                        </div>
                    {{-- <div class="priduct_social">
                        <ul>
                            <li><a class="facebook" href="product-details.html#" title="facebook"><i class="fa fa-facebook"></i> Like</a></li>
                            <li><a class="twitter" href="product-details.html#" title="twitter"><i class="fa fa-twitter"></i> tweet</a></li>
                            <li><a class="pinterest" href="product-details.html#" title="pinterest"><i class="fa fa-pinterest"></i> save</a></li>
                            <li><a class="google-plus" href="product-details.html#" title="google +"><i class="fa fa-google-plus"></i> share</a></li>
                            <li><a class="linkedin" href="product-details.html#" title="linkedin"><i class="fa fa-linkedin"></i> linked</a></li>
                        </ul>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>
</div>
<!--product details end-->

@endsection
    