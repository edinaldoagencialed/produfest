@extends('layouts.default')
@section('content')

<!--slider area start-->
<section class="slider_section mb-78">
    <div class="slider_area owl-carousel">
        
        
        <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset('img/slider/slider5.jpg') }}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="slider_content">
                            <h2 class="text-right natal-title">A ALEGRIA <br> DO NATAL</h2>
                             <h1 class="text-right natal-subtitle mt-2">CHEGOU POR AQUI</h1>
                             <!--<p>Novidades que diferenciam seu negócio</p>-->
                             <a class="button natal-link" href="/categoria/linha-natal">CONHEÇA AS NOVIDADES!</i></a>
                         </div>
                    </div>
                </div>
            </div>
         </div>
        
        
        <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset('img/slider/slide1.jpg') }}">
           <div class="container">
               <div class="row">
                   <div class="col-12">
                       <div class="slider_content">
                           <h2 class="text-right">Realize</h2>
                            <h1 class="text-right"> o momento</h1>
                            <!--<p>Novidades que diferenciam seu negócio</p>-->
                            <!--<a class="button" href="/subcategoria/tematico">CONFIRA <i class="fa fa-angle-double-right"></i></a>-->
                        </div>
                   </div>
               </div>
           </div>
        </div>
        
        <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset('img/slider/slide2.jpg') }}">
           <div class="container">
               <div class="row">
                   <div class="col-12">
                       <div class="slider_content">
                           <h2>Novidade!</h2>
                            <h1>Porta-doces temáticos</h1>
                            <!--<p>Novidades que diferenciam seu negócio</p>-->
                            <a class="button" href="/subcategoria/tematico">CONFIRA <i class="fa fa-angle-double-right"></i></a>
                        </div>
                   </div>
               </div>
           </div>
        </div>
        
        <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset('img/slider/slide3.jpg') }}">
           <div class="container">
               <div class="row">
                   <div class="col-12">
                       <div class="slider_content">
                           <h2>Com a Produfest</h2>
                            <h1>você faz festa para as vendas.</h1>
                            <!--<p>Novidades que diferenciam seu negócio</p>-->
                            <!--<a class="button" href="/subcategoria/tematico">CONFIRA <i class="fa fa-angle-double-right"></i></a>-->
                        </div>
                   </div>
               </div>
           </div>
        </div>
        
        <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset('img/slider/slide4.jpg') }}">
           <div class="container">
               <div class="row">
                   <div class="col-12">
                       <div class="slider_content">
                           <h2 class="text-right">Boleiras</h2>
                            <h1 class="text-right">multifunções - 4 em 1</h1>
                            <!--<p>Novidades que diferenciam seu negócio</p>-->
                            <!--<a class="button" href="/subcategoria/tematico">CONFIRA <i class="fa fa-angle-double-right"></i></a>-->
                        </div>
                   </div>
               </div>
           </div>
        </div>

        
        </div>
    </div>
</section>

<!--slider area end-->

<!--banner area start-->
{{-- <div class="banner_area mb-78">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-lg-6 col-md-6">
                <figure class="single_banner box_1 mb-15">
                    <div class="banner_thumb">
                        <a href="shop.html"><img src="{{ asset('img/banner1.jpg') }}" alt=""></a>
                    </div>
                </figure>
            </div>
            <div class="col-lg-6 col-md-6">
                <figure class="single_banner box_2">
                    <div class="banner_thumb">
                        <a href="shop.html"><img src="{{ asset('img/banner1.jpg') }}" alt=""></a>
                    </div>
                </figure>
            </div>
        </div>
    </div>
</div> --}}
<!--banner area end-->



<!--featured products area start-->
@foreach ($categories as $category)
    

<div class="featured-products mb-78">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                        <h2>{{ $category->name }}</h2>
                </div>
                <div class="product_carousel product_column5 owl-carousel">
                    
                    @foreach ($category->products as $product)
                        
                    <article class="single_featured">
                        <figure>
                            <div class="product_thumb">
                                <a href="product-details.html">
                                    @php
                                        $file = public_path().'/uploads/produtos/'.$product->image;
                                    @endphp
                                    @if(file_exists($file))
                                        <img id="zoom1" src="{{ URL::to('uploads/produtos/'.$product->image) }}" data-zoom-image="{{ URL::to('uploads/produtos/'.$product->image) }}" alt="">
                                    @else
                                        <img src="{{ URL::to('uploads/produtos/noimage.jpg') }}" alt="$item->image">
                                    @endif
                                </a>
                                <div class="action_links">
                                    <ul>
                                        {{-- <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="pe-7s-cart"></i></a></li> --}}
                                        <li class="quick_button"><a href="{{ route('front::product',['url'=>$product->url]) }}" {{-- data-toggle="modal" --}} data-target="#modal_box"  title="Vizualizar"> <i class="pe-7s-search"></i></a></li>
                                        {{-- <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="pe-7s-like"></i></a></li> --}}
                                    </ul>
                                </div>
                                <div class="featured_content">
                                    <h4 class="featured_name"><a href="{{ route('front::product',['url'=>$product->url]) }}">{{ $product->name }}</a></h4>
                                    
                                </div>
                            </div>
                        </figure>
                    </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<!--featured products area end-->    

@endsection
