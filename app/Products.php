<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $guarded = [];
    protected $fillable = ['id','name','description','image','url','categories_id','subcategories_id'];
    public $timestamps = false;
    protected $table = 'products';

    public function variations(){
        return $this->hasMany('App\Variations');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_products','products_id','categories_id');
    }


}
