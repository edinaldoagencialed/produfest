<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Products;
use App\Subcategories;
use App\Variations;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    //Importar as categorias
    public function categories()
    {
        $handle = fopen('categories.csv', "r");
        $header = true;
        echo "<pre>";
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            if ($header) {
                $header = false;
            } else {
                print_r($csvLine);
                $entity = new Categories();
                //Inserir no banco de dados
                $data = [
                    'id' => $csvLine[0],
                    'name' => $csvLine[1],
                    'url' => $this->url_verify($csvLine[1], $entity),
                ];

                $entity->create($data);
            }
        }
        echo "</pre>";
    }

    public function subcategories()
    {
        $handle = fopen('subcategories.csv', "r");
        $header = true;
        //echo "<pre>";
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            if ($header) {
                $header = false;
            } else {
                print_r($csvLine);
                $entity = new Subcategories();
                //Inserir no banco de dados
                $data = [
                    'id' => $csvLine[0],
                    'name' => $csvLine[1],
                    'url' => $this->url_verify($csvLine[1], $entity),
                    'categories_id' => $csvLine[3]
                ];

                $entity->create($data);
            }
        }
        //echo "</pre>";
    }


    public function products()
    {
        $model = new Products();
        $variations = new Variations();

        #$handle = fopen('csv/bandejas.csv', "r");
        #$handle = fopen('csv/base_ferro.csv', "r");
        #$handle = fopen('csv/boleiras.csv', "r");
        #$handle = fopen('csv/porta-doces.csv', "r");
        #$handle = fopen('csv/porta-tubetes.csv', "r");
        #$handle = fopen('csv/pratos.csv', "r");
        #$handle = fopen('csv/torres.csv', "r");
        ###$handle = fopen('csv/miniaturas.csv', "r");
        ##$handle = fopen('csv/porta-baloes.csv', "r");
        
        ##$handle = fopen('csv/porta-baloes-2.csv', "r");
        ##$handle = fopen('csv/porta-doces-2.csv', "r");
        
        
        
        
        $header = true;
        $aa = 0;
        echo "<pre>";
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            print_r($csvLine);
            if ($header) {
                $header = false;
            } else {
                if ($csvLine[0] == 1) {
                    // $data = [
                    //     'name' => strtoupper($csvLine[1]),
                    //     'sku' => $csvLine[0],
                    //     'description'=>$csvLine[2]."<br />".$csvLine[3],
                    //     'image' => ((isset($csvLine[7])) and (trim($csvLine[7])!=""))?$csvLine[7].".jpg":NULL,
                    //     'categories_id' => $csvLine[5],
                    //     'subcategories_id' => ((isset($csvLine[6])) and (trim($csvLine[6])!=""))? $csvLine[6] : NULL,
                    //     'url' => $this->url_verify($csvLine[1], $model)
                    // ];

                    $data = [
                        'name' => strtoupper($csvLine[1]),
                        'sku' => $csvLine[0],
                        'description'=>$csvLine[2]."<br />".$csvLine[3]."<br />".$csvLine[4],
                        'image' => ((isset($csvLine[8])) and (trim($csvLine[8])!=""))?$csvLine[8].".jpg":NULL,
                        'categories_id' => $csvLine[6],
                        'subcategories_id' => ((isset($csvLine[7])) and (trim($csvLine[7])!=""))? $csvLine[7] : NULL,
                        'url' => $this->url_verify($csvLine[1], $model)
                    ];

                    $created = $model->create($data);
                    $principal_id = $created->id;
                } else {

                    // $va = [
                    //     'sku'=>$csvLine[0],
                    //     'products_id'=>$principal_id,
                    //     'description'=>$csvLine[2]."<br />".$csvLine[3],
                    //     'image'=>((isset($csvLine[7])) and (trim($csvLine[7])!=""))?$csvLine[7].".jpg":NULL,
                    // ];

                    $va = [
                        'sku'=>$csvLine[0],
                        'products_id'=>$principal_id,
                        'description'=>$csvLine[2]."<br />".$csvLine[3]."<br />".$csvLine[4],
                        'image'=>((isset($csvLine[8])) and (trim($csvLine[8])!=""))?$csvLine[8].".jpg":NULL,
                    ];


                    $variations->create($va);
                }
                
            }
        }
        echo $aa;
        echo "</pre>";
    }


    //Verificação de URL
    protected function url_verify($string, $model, $id = '')
    {
        $slugfy = $this->slugify($string, '-');

        //Caso não tenha passado o id
        if ($id == "") {
            $url_result_verify = $model->where('url', $slugfy)->get()->first();
            if (isset($url_result_verify)) {
                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model->where('url', '=', $url_new)->first();
                    if (empty($url_result_verify)) {

                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        } else {
            $url_result_verify = $model
                ->where('url', $slugfy)
                ->where('id', '<>', $id)
                ->first();

            if (isset($url_result_verify)) {

                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model
                        ->where('url', $url_new)
                        ->where('id', '<>', $id)
                        ->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        }
    }

    //URL Amigável
    public function slugify($text)
    {
        $string = preg_replace('/[\t\n]/', ' ', $text);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $list = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        );

        $string = strtr($string, $list);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);

        return $string;
    }
}
