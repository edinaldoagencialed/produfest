<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function home(){

        //Listar Produtos das categorias aleatoriamente
        $categories = Categories::with(['products'=>function ($q){
            $q->where('image','<>',NULL)->inRandomOrder();
        }])->where('home','=',1)->limit(5)->get();
        
        return View('home',[
            'categories'=>$categories
        ]);
    }

    public function about(){
        return View('about');
    }


    public function catalogo() {
        return View('catalogo');
    }

    public function contato() {
        return View('contato');
    }

    public function catalog(){
        return View('catalog');
    }

    public function produtos(){
        return View ('produtos');
    }

    public function productdetails(){
        return View ('productdetails');
    }


    
}
