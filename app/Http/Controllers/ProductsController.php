<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

use Maatwebsite\Excel\Facades\Excel;
use App\Categories;

class ProductsController extends Controller
{


    public function detail($request)
    {
        $product = Products::with('variations')->where('url', $request)->first();
        return View('products.detail', ['_product' => $product,'_subtitle'=>$product->name]);
    }

    public function search()
    {

        $term = $_GET['s'];
        $result = Products::where('name', 'like', '%' . $term  . '%')
            ->orWhere('sku', $term)
            ->orWhere('description', 'like', 'like', '%' . $term  . '%')
            ->paginate(6);

        return View('products.search', [
            '_term' => $term, 
            'products' => $result
        ]);
    }



    //Importar as categorias
    public function categorias()
    {
        $handle = fopen('categorias.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ";")) {

            
            if ($header) {
                $header = false;
            } else {
                print_r($csvLine);
                $model = new Categories();
                
                $data = [
                    'id' => $csvLine[0],
                    'name' => $csvLine[1],
                    'categories_id' => $csvLine[2],
                    'url' => $this->slugify($csvLine[1])
                ];
                //Categories::create($data);
            }
        }
    }

    //Importar os produtos
    public function produtos()
    {
        $handle = fopen('produtos.csv', "r");
        $header = true;
        echo '<pre>';
        $model = new Products();
        while ($line = fgetcsv($handle, 1000, ";")) {
            if ($header) {
                $header = false;
            } else {
                $data = [
                    'id'=>$line[0],
                    'name'=>$line[1],
                    'description'=>$line[2],
                    'image'=>$line[3],
                    'url'=>$this->slugify($line[1])
                ];

                // $entity = Products::create($data);
                
                // //Insert n:n
                // $cat = explode(',',$line[4]);
                // foreach($cat as $i){
                //     $entity->categories()->attach($i);
                // }

            }
        }
    }

    public function urltransform()
    {
        $model = new Products();
        $produtcs = $model->all();
        foreach ($produtcs as $product) {
            //echo $product->name;
            $url = $this->url_verify($product->name, $model, $product->id);
            $data = ['url' => $url];
            $product->update($data);
        }
    }

    //Verificação de URL
    protected function url_verify($string, $model, $id = '')
    {
        $slugfy = $this->slugify($string, '-');

        //Caso não tenha passado o id
        if ($id == "") {
            $url_result_verify = $model->where('url', $slugfy)->get()->first();
            if (isset($url_result_verify)) {
                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model->where('url', $url_new)->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        } else {
            $url_result_verify = $model
                ->where('url', $slugfy)
                ->where('id', '<>', $id)
                ->first();

            if (isset($url_result_verify)) {

                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model
                        ->where('url', $url_new)
                        ->where('id', '<>', $id)
                        ->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        }
    }

    //Remover hifens e espaços
    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    //URL Amigável
    public function slugify($text)
    {
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
}
