<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

/* Model Uses*/
use App\Categories;
use App\Products;

class CategoriesController extends Controller
{
    public function index($request)
    {
        //Verifica se existe subcategorias, se existir, mostra o menu das subcategorias
        $category = Categories::with('products','subcategories')
        
        ->where('url', $request)->first();

        //paginacao
        $items = $category->products()->where('products.image','<>',NULL)->paginate(6);

        return view('categories.products',[
            '_subtitle'=>$category->name,
            'category' => $category,
            'products' => $items
        ]);
    }
}
