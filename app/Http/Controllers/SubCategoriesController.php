<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

/* Model Uses*/
use App\Categories;
use App\Products;
use App\SubCategories;

class SubCategoriesController extends Controller
{
    public function index($request)
    {
    
        //Verifica se existe subcategorias, se existir
        $subcategory = Subcategories::has('category')->where('url', $request)->first();
        
        $sub = SubCategories::where('categories_id',$subcategory->categories_id)->get();
        
        //paginacao
        $items = $subcategory->products()->where('products.image','<>',NULL)->paginate(6);

        return view('subcategories.products',[
            '_subtitle' => $subcategory->category->name.' - '.$subcategory->name,
            'subcategories' => $sub,
            'category' => $subcategory,
            'products' => $items
        ]);
    }
}
