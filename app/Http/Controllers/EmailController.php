<?php

namespace App\Http\Controllers;

use App\Leads;
use App\Mail\SendCatalogo;
use App\Mail\SendMail;
use App\Mail\SendNewsletter;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function email(Request $req)
    {
        $form = $req->all();
        $subject = $form['subject'];
        Mail::to('contato@produfest.com.br')
            ->send(new SendMail($form, $subject));

        if (Mail::failures()) {
            echo '0';
            exit;
        }
        echo '1';
    }

    public function sendmailcatalogo(Request $req)
    {
        $form = $req->all();
        Mail::to(['enzo.nagata@gmail.com', $form['email']])
            ->send(new SendCatalogo($form));

        if (Mail::failures()) {
            echo '0';
            exit;
        }
        //Cadastrar Leads
        Leads::create($form);

        echo '1';
    }

    public function newsletter(Request $req)
    {
        $email = $req->input('email');

        //verifica se o e-mail já foi cadastrado
        $verify_email = Newsletter::where('email', '=', $email)->get();

        if ($verify_email->count() <= 0) {
            //Cadastra o e-mail
            if (Newsletter::create(['email' => $email])) {

                Mail::to('contato@produfest.com.br')
                    ->send(new SendNewsletter(['email' => $email]));
                if (Mail::failures()) {
                    echo '0';
                    exit;
                }
                echo '1';
            } else {
                echo '0';
            }
        } else {
            echo '2';
        }
    }
}
