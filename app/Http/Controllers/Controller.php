<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //Todas as categorias
        $categories = Categories::with(['subcategories'=>function($q){
            $q->orderBy('name','asc');
        }])->orderBy('name','asc')->get();
        View::share ( '_categories',$categories);
    }
}
