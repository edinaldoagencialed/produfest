<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\View;
use App\Categories;

class GlobalDatas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
        * Define variávies globais
        */
        View::share('_title','Produfest');

    

        return $next($request);
    }
}
