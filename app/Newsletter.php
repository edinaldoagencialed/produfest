<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $guarded = [];
    protected $fillable = ['id', 'email'];
    public $timestamps = false;
    protected $table = 'newsletter';
}
