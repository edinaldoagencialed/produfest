<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $guarded = [];
    protected $fillable = ['id','nome','email','cnpj'];
    protected $table = 'leads';
    public $timestamps = false;

}
