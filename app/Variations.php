<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variations extends Model
{
    protected $guarded = [];
    protected $fillable = ['id', 'products_id', 'description', 'image','sku'];
    protected $table = 'variations';
    public $timestamps = false;

}
