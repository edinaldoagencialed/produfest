<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $guarded = [];
    protected $fillable = ['id','url','name','image','color','url','categories_id','columns'];
    protected $table = 'categories';
    public $timestamps = false;

    public function products(){
        return $this->hasMany('App\Products');
    }

    public function subcategories(){
        return $this->hasMany('App\Subcategories');
    }
}
