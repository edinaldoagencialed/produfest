<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategories extends Model
{
    protected $guarded = [];
    protected $fillable = ['id', 'url', 'name', 'image', 'color', 'url', 'categories_id', 'columns'];
    protected $table = 'subcategories';
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Categories','categories_id');
    }

    public function products()
    {
        return $this->hasMany('App\Products');
    }
}
